#!/usr/bin/env python3

from __future__ import with_statement

import os
import sys
import errno

# pip3 install fusepy
from fusepy import FUSE, FuseOSError, Operations



INSTANCE = None
class Passthrough(Operations):

    def __init__(self, root):
        global INSTANCE
        self.root = root
        INSTANCE=self
    # GIT Commands


    def git_pull(self):
        cwd=os.getcwd()
        os.chdir(self.root)
        
        os.system("git pull")

        os.chdir(cwd)
    
    def git_push(self,message):
        cwd=os.getcwd()
        os.chdir(self.root)

        os.system("git add -A")
        os.system(f"git commit -am {message}")
        os.system("git push")

        os.chdir(cwd)


    def git_wrap(f):
        global INSTANCE

        def newf(*args,**kwargs):
            global INSTANCE
            INSTANCE.git_pull()
            results= f(*args,**kwargs)
            INSTANCE.git_push(f.__name__)
            return results       

        return newf

    # Helpers
    # =======

    def _full_path(self, partial):
        partial = partial.lstrip("/")
        path = os.path.join(self.root, partial)
        return path

    # Filesystem methods
    # ==================

    @git_wrap
    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def chmod(self, path, mode):
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, path, fh=None):
        full_path = self._full_path(path)
        st = os.lstat(full_path)
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

    @git_wrap
    def readdir(self, path, fh):
        full_path = self._full_path(path)

        dirents = ['.', '..']
        if os.path.isdir(full_path):
            dirents.extend(os.listdir(full_path))
        for r in dirents:
            yield r

    @git_wrap
    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    @git_wrap
    def mknod(self, path, mode, dev):
        return os.mknod(self._full_path(path), mode, dev)

    @git_wrap
    def rmdir(self, path):
        full_path = self._full_path(path)
        return os.rmdir(full_path)

    @git_wrap
    def mkdir(self, path, mode):
        return os.mkdir(self._full_path(path), mode)

    def statfs(self, path):
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    @git_wrap
    def unlink(self, path):
        return os.unlink(self._full_path(path))

    @git_wrap
    def symlink(self, name, target):
        return os.symlink(name, self._full_path(target))

    @git_wrap
    def rename(self, old, new):
        return os.rename(self._full_path(old), self._full_path(new))

    @git_wrap
    def link(self, target, name):
        return os.link(self._full_path(target), self._full_path(name))

    def utimens(self, path, times=None):
        return os.utime(self._full_path(path), times)

    # File methods
    # ============

    @git_wrap
    def open(self, path, flags):
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    @git_wrap
    def create(self, path, mode, fi=None):
        full_path = self._full_path(path)
        return os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)

    @git_wrap
    def read(self, path, length, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    @git_wrap
    def write(self, path, buf, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.write(fh, buf)

    @git_wrap
    def truncate(self, path, length, fh=None):
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    @git_wrap
    def flush(self, path, fh):
        return os.fsync(fh)

    @git_wrap
    def release(self, path, fh):
        return os.close(fh)

    @git_wrap
    def fsync(self, path, fdatasync, fh):
        return self.flush(path, fh)


if __name__ == '__main__':
    if len(sys.argv)!=3:
        print("""Usage : gafs.py mountpoint git_dir

Please clone your repo and switch to a private branch in such a way that git pull/commit/push would NEVER ASK YOU ANYTHING.

For instance, log with http://USER:PASS@domain/repo/file.git or with SSH""" )
        sys.exit(-1)
    mountpoint = sys.argv[1]
    git_dir = sys.argv[2]
    FUSE(Passthrough(git_dir), mountpoint, nothreads=True, foreground=True)
    
    
"""
https://www.stavros.io/posts/python-fuse-filesystem/
pip install fusepy

git clone http://user123:user123@localhost:3000/user123/test.git
clone comme ça ou avec SSH pour que ça demande jamais les identifiants

Gitea pour tests locaux
https://gitea.com/

Détecteur de lancement en root ; sinon erreur
"""

# Git As A Filesystem
You're a busy person and you have no time to lose using Git ? 

Use Git-As-A-Filesystem ! Our cutting-edge quantum-powered filesystem allows you to synchronize your filesystem with a Git repo without the hassle of typing commands. Its blocking architecture allows you to always have the latest data.

/!\ Data loss is intended and is provided as a feature.

We're currently working on the slight delays introduced by the sync' mechanism.

First, as root, # pip install fusepy

Usage : gafs.py mountpoint git_dir

Please clone your repo and switch to a private branch in such a way that git pull/commit/push would NEVER ASK YOU ANYTHING.
For instance, log with http://USER:PASS@domain/repo/file.git or with SSH

Run as root. (because FUSE)

![Animation](demo.gif)
This software is a joke and - even though it works - it should not be used in any context.
